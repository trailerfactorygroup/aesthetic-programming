let events;
let comment;
let i_did;
let e, c, i;
let events_x;
let events_y;
let comment_x;
let comment_y;
let i_did_x;
let i_did_y;
let screen_ = 0;
let countS = 0;
let randomEvents;
let randomComment;
let randomI_did;
let speakEvents = [];
let speakComment = [];
let speakI_did = [];


function preload() {
  // loads the JSON files into the sketch
  events = loadJSON("events.json");
  comment = loadJSON("comment.json");
  i_did = loadJSON("i_did.json");

  /* loads the sound. This is done with a for-loop that runs through each sound file in the folder: voices.
  It needs to be from 0 to 15 because we count from 0 and 15 is not included*/
  for (let i = 0; i < 15; i++){
    speakEvents[i] = loadSound("voices/events"+i+".wav")
    }

  for (let i = 0; i < 15; i++){
    speakComment[i] = loadSound("voices/comment"+i+".wav")
    }

  for (let i = 0; i < 15; i++){
    speakI_did[i] = loadSound("voices/idid"+i+".wav")
  }
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(15);
  textFont('Georgia');
}

function draw() {

if (screen_ == 0){ // start screen (a conditional statement concerning a global variable that changes the screens)
  fill(255);
  textSize(52);
  textAlign(CENTER);
  text('WHILE WE SLEPT', width/2, 200); // title
  textSize(20);
  fill(150);
  noStroke();
  text('A critiqual artwork addressing the war in Ukraine,', width/2, 270); // info texts
  text('however, the actors have all been removed,', width/2, 320 );
  text('and while we sleep the war rages on', width/2, 370 );
  fill(90);
  text('- press the mouse for a new event -', width/2, height-50); // mousePressed text
  }

if (screen_ == 1){ // main screen (a conditional statement concerning a global variable that changes the screens)
  background(0);
  countS++ // a global variable that increments which each frameCount (used to control when to display the end quotes and stop the sound)
  push();
  textAlign(CENTER);
  fill(255);
  textSize(42);
  text("WHILE WE SLEPT. . .", 600, 170) // title
  pop();
// displays texts from the JSON files
  push();
  textAlign(CENTER);
  fill(255);
  textSize(30);
  text(e, events_x, events_y);
  text(c, comment_x, comment_y);
  text(i, i_did_x, i_did_y);
  // this syntax makes the texts shake
    events_x = floor(random(width/2,width/2+3));
    comment_x = floor(random(width/2,width/2+3));
    i_did_x = floor(random(width/2,width/2+3));
    events_y = floor(random(280,283));
    comment_y = floor(random(380,383));
    i_did_y = floor(random(480,483));
  pop();

  textSize(20);
  fill(90);
  text('- press the mouse for a new event -', width/2, height-50); // mousePressed text

  if (countS >=100){ // end screen 1 (countS is a gloabl variable connected to frameCount that controls when the end quote will be displayed).
      background(0);
      fill(255);
      textSize(40);
      textAlign(CENTER);
      text('"In war, the first victim becomes the truth"', width/2, height/2);
    }

  if (countS > 180){ // end screen 2 (countS is a gloabl variable connected to frameCount that controls when the end quote will be displayed)
      background(0);
      fill(255);
      textSize(40);
      textAlign(CENTER);
      text('"But what is the truth?"', width/2, height/2);
    }
  }
}

function mousePressed() {
  screen_ = 1; // changes the global variable screen_ from 0 to 1 when the mouse pressed (can only do this once).

  /*syntax that chooses a random integer each time the frameRate gets refreshed and puts it inside the array when mouse is pressed.
  Since the order of the JSON files and sound files match each other, both arrays will have the same index value*/
  randomEvents = floor(random(0,15)); // it needs to be from 0 to 15 because we count from 0 and 15 is not included
  randomComment = floor(random(0,15));
  randomI_did = floor(random(0,15))
  e = events.events[randomEvents];
  c = comment.comment[randomComment];
  i = i_did.i_did[randomI_did];
  /*syntax that displays the sound, however, there is a delay on the last to sentences which is made with the setTimeout function*/
  speakingNowEvents();
  setTimeout(speakingNowComment,6000);
  setTimeout(speakingNowI_did,12000);

}
/*self-made functions that contain a syntax which play the sound and
 a conditional statements that only allow the sound to play if the variable countS is under 600. */

function speakingNowEvents(){
  if (countS < 600){
    speakEvents[randomEvents].play();
  }
}

function speakingNowComment(){
  if (countS < 600){
  speakComment[randomComment].play();
  }
}

function speakingNowI_did(){
  if (countS < 600){
  speakI_did[randomI_did].play();
  }
}
