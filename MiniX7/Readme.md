## Revisit MiniX  

I have chosen to revisit the miniX1, Love letter.

All of the code was altered, but the conception remained the same. It was Valentine's Day by the time we handed off the first miniX, and as we were handed  a free task, I was inspired by the celebration of romance and love.
As said, I altered almost everything because the code was so simple that it was difficult to go elaborate on it. The heart and the color scheme are the only elements that have been kept. I wanted to keep working on miniX1 this week because I'd been thinking about it since first week. I had great ideas from the beginning of the subject, but it was difficult for me to translate them into code.  As a result, I'd like to experiment with a more advanced sketch to put an end to the ideas I've had since then.

Since the beginning of the subject, I've learnt a lot of new syntaxes, and I'd like to try to incorporate some of them into the initial sketch. I had a lot of trouble getting my goods to operate in my miniX6, but I concentrated on finishing a class. It was a huge win for me as it worked, therefore I implemented it in this sketch as well.

![](RevisitLoveLetter.png)

#### What does aesthetic programming have to do with digital culture?
Aesthetic Programming is about the larger social, cultural, and political consequences of programming and computation, as Soon and Cox emphasize in the book's foreword. Instead of focusing solely on the practical aspects of coding, the book also addresses the aforementioned dimensions in order to illuminate programming as a tool for thinking and acting (preferably critically). The method tries to expand present limitations within programming fields where the focus is frequently exclusively on learning the 'how to' by drawing on the field of Software Studies.

1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX7/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX7/sketch.js)
