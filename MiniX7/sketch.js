const hearts = [];

function setup() {
  createCanvas(600, 400);
}

function draw() {
  background('#fae');
  for (let i = 0; i < 3; i++){
    let b = new Heart();
    hearts.push(b);
  }
  for (let i = hearts.length - 1; i >= 0; i--) {
    hearts[i].update();
    hearts[i].show();
    if (hearts[i].finished()) {
      hearts.splice(i, 1);
    }
  }
}

 function heart (x, y, size) {
  beginShape();
  vertex(x, y);
  bezierVertex(x-size/2, y-size/2, x-size, y+size/3, x, y+size);
  bezierVertex(x+size, y+size/3, x+size/2, y-size/2, x, y);
  endShape(CLOSE);
}

class Heart {

  constructor() {
    this.x = mouseX;
    this.y = mouseY;
    this.vx = random(-1, 1);
    this.vy = random(-6, -1);
    this.alpha = 200;
  }

  finished() {
    return this.alpha < 0;
  }

  update() {
    this.x += this.vx;
    this.y += this.vy;
    this.alpha -= 0.5;
  }

  show() {
    noStroke();
    fill(random(300), random(30), 60, this.alpha);
    heart(this.x, this.y, 20);
  }

}
