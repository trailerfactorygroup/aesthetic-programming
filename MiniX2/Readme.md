## MiniX2
Projekt this week was emojis
I created: Clown Festival

1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX2/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX2/sketch.js) 

#### What have I produced?

For this miniX i have produced, what i call _Clown festival_.
Really, it's just two clowns with two different expressions, that gives the recipient two different expressions to interpret. It is meant as a fun feature of the conversation. I would argue that they can be used in many different contexts.

![](ClownFestivalPicture.png)

The idea behind the Clown Festival was to elaborate on last week's work, whereas we created an ellipse. That is why the shape of the clown's head is made only by using the syntax: ellipse. The common geometric shapes is triangles, rectangles and squares.  
I explored these different shapes, but also sizes and positions. The fun arose in manipulating those properties and reconstructing them in a new way that even created a new motif.

I have not yet thought of geometry as anything other than mathematics, but after this MiniX, it is clear to me that geometry is to be found in pretty much everything - signage, typography, graphic design, etc.

I have not dived much into to the political perspective of emojis. However, I have used them diligently to express myself emotionally using the “states” the emoji has on its face. I see, it's fairly consistent in all the communication I exchange with my friends, but also the way I see people express themselves in general on facebook or other media.
This Minix has made me realise that representation policy issues can arise. It is almost as if the more one tries to expand the universalism of emojis, the more the situation worsens. This is due to the the exclusion - it becomes clearer which people you involve and which ones you omit, the more emojis that get made.

Personally, I do not believe that the solution to the representation policy problems is to create more emojis. Instead I think that the solution is to use the emojis in different ways. And since we are discussing language in both aesthetic programming and software studies, perhaps some parallels could be drawn. We say that "_Writing and coding are deeply entangled, and neither should be privileged over the other: we learn from their relationally." and in relation to writing and using emojis we say that "_Unicode is clearly important to communicative operations across international/multilingual systems._" My point is that these statements are quite similar in their wording. I think it would interesting to accept emojis as a language.
Else does emojis need to be more customised so that users can define their emojis themselves. But in that way, problems might arise in the way we understand each others emojis -  because there is no common understanding of how we express ourselves with emojis.

###### References
(Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming, p. 13-24)
