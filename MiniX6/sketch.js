let circles = [];
let hero;
let h;
let c;
let lose = 0;

// I used the preload funtion to handle the asynchronous loading of external files.
function preload () {

  // h: this preload is the hero-character.
  h =loadImage('Hero.png')
  // c: this is the city placed in the background.
  c =loadImage('city.png')
}
function setup() {
  createCanvas(windowWidth,windowHeight);
  // here I call for the class I define later on.
  hero = new Hero ();

// I create a 'for loop' to get even more circles, placed randomly.
  for(let i = 0; i < 20; i++) {
    circles[i] = new Circle(random(width), random(height), random(-3, 3), random(-3, 3));

  }

}

function draw() {
  background(0);

// I used imageMode to modify the location of the image.
  // CENTER interprets the second and third parameters of the image, as the image's center point.
  imageMode(CENTER)
  image(c,windowWidth/2, windowHeight-200)


  for(let i = 0; i < circles.length; i++) {
    circles[i].move();
    circles[i].display();
  }

  hero.display();

// I used the keyIsDown funtion to check if the key is currently down pressed.
  // It is usefull when you have an object (the character) that moves
    // because you may want several keys to be able to affect the characters behaviour simultaneously.

  // I use the function to check if arrow keys are pressed.
  if (keyIsDown(LEFT_ARROW)){
    hero.x -= 10;
  }
  if (keyIsDown(RIGHT_ARROW)){
    hero.x += 10;
  }
  if (keyIsDown(UP_ARROW)){
    hero.y -= 10;
  }
  if (keyIsDown(DOWN_ARROW)){
    hero.y += 10;
  }

// I call for the funtion in the below.
  checkHit();

}

// Here I create a function, that checks if hero hits the circles.
function checkHit(){
    // Calculates the distance between each circle
  for (let i =0; i < circles.length; i++){
    let d = int(dist(hero.x, hero.y, circles[i].x, circles[i].y,));
    if (d < hero.size.h/2){
      lose++
      // I use splice() to remove the circles when hitten.
      circles.splice(i,1);
    }
  }
}


// I create a circle class
class Circle {
  constructor(x, y, xSpeed, ySpeed) {
    this.x = x;
    this.y = y;
    this.xSpeed = xSpeed;
    this.ySpeed = ySpeed;
    this.diameter = random(10,30)

  }

  // It instantiate the circles to move around the screen by using the parameters set in the class.
  move() {
    this.x += this.xSpeed;
    if (this.x < 0 || this.x > width) {
      this.xSpeed *= -1;
    }

    this.y += this.ySpeed;
    if (this.y < 0 || this.y > height) {
      this.ySpeed *= -1;
    }
  }

  display() {
    circle(this.x, this.y, this.diameter);
  }

}

// I create a hero class
class Hero {
constructor(){
  this.x = width/2;
  this.y = height - 100;
  this.size = {w: 100, h: 100};

}

  display() {
    image(h,this.x,this.y,this.size.w,this.size.h)
  }
}
