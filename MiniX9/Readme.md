
#### **Group flowchart**

<img src="MiniX9/Group_Flowchart.png">

#### **Idea 1 - Desktop Friend**

Our first idea is to create a program that simulates a Desktop Friend. The background will look like a normal desktop with a simple game in the middle which functions as a simulation of using a computer over time. The game is also there to keep the user interacting with the program for a longer time. In the lower right corner of the screen the Desktop Friend will introduce itself in a friendly way. During the running time of the program the DF will make comments to the user and this happens when and when a certain amount of time has passed and every time the user wins a level in the game. Initially the comments are very nice and encouraging, showing that the DF is your friend. But, if the Desktop Friend is clicked with the cursor the DF will make an annoyed comment and temporarily change costume to an annoyed version. If the user chooses to keep on annoying the DF, the DF comments on the game and the user in general will increasingly become more and more wicked. At the end the screen will freeze and chaos will occur on the screen. 

#### **Idea 2 - Quiz city**

Our second idea is a game, which at first glance seems harmless but hides some underlying sinister intentions. Firstly the program presents the player with a survey or quiz in which they will choose one of multiple images to answer the questions. The program collects data based on these answers and creates a ‘stereotype’ for the player. When the quiz is over, the player is presented with a map of a city with clickable ‘establishments'. The stereotype uses biases to determine which establishments the player is allowed to enter – e.g. if you enjoy classical music and fine wine, you won’t be allowed into the tattoo-shop. If you are allowed into the establishment, visuals will pop up on the screen, as if you entered the building – e.g. when you enter the hairdresser’s, the webcam will show your image as if it was a mirror. The intention of the game is to highlight the discrimination that occurs in relation to wealth, race and status.



### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

We found that one has to make a choice of what to include in relation to what the flowchart is to be used for. Difficulties involved in this process could be selecting the important actions as well as keeping an overview of the functions and syntax of the program. Our group flowcharts ended up having a more communicative angle, because we used the flowchart to determine how we wanted the program to function. One challenge that can occur with this approach is the ambitious ideas that may arise, when one focuses on the concept alone without taking the technical aspects into consideration. We imagine that as we start writing the code, we will gain a more technical and practical understanding of our program, which likely will result in a more technical flowchart. We have an expectation that the only way for us to make a completely representative flowchart, is to finish writing the code before the final flowchart is made. This is because one can have an idea of a solution, but end up going in a different direction, when one is in the coding process. 


### What are the technical challenges facing the two ideas and how are you going to address these?**

**Idea 1:**

A challenge for this program is to make the Desktop Friend appear “real”.  

*_Solution:_ Adding different “costumes” that Desktop Friend can change between to create an animated feel.

*_Solution:_ Making the DF to an object and assigning attributes to it. 


Another challenge is how to connect the level up in the game to DF’s comments 

*_Solution:_ Make a conditional statement that checks the a “score” variable and calls a comment from the JSON file when “scores” changes (when level up) 


A third challenge is to create a “mood” function that determines which JSON file is referred to (each file containing different kinds of comments). 

*_Solution:_ Create a counter that adds one every time the Desktop Friend is clicked. Then using a conditional statement to check the counted number and then calling the JSON files accordingly. 


Another not so technical challenge is how we can nudge the user to stay long enough to discover the changing mood of the Desktop Friend and consequently changes in its attitude.  

*_Solution:_ Make the game simple enough to easily complete levels which will create immediate feedback/reward and make progressing in the program happen faster. 



**Idea 2:**

Our main concern is how to create the different categories and how to store the user data when an image is clicked as this will be the mechanism giving the user access to the building on the map.

*_Solution:_ assign the images a category and create a variable in relation to each category that increments, when the user clicks on the image. Use conditional statements to connect selected categories to the buildings on the map.


We have to decide what pictures represent the desired stereotype which relates to how many points one gets and where they gain access. 

*_Solution:_ We need to make a decision about how many times one must pick a category in order to gain access to a building. 


Another concern is how to connect the value system with the building on the map and show whether or not access is granted.

*_Solution:_ create a function for each building and use a conditional statement  that gives access to the building (changes screens) if certain criteria is met. 


To elevate the program, we want to incorporate sound when one enters a building or experiences a certain event.

*_Solution:_ we need to access the P5.js sound library and connect the inside of each building (the screen) to a sound through conditional statements.


To expand the storyline, we want to incorporate events that depend on the value system but get triggered by a preset condition

*_Solution:_ we can make things pop up depending on a trigger (such as time, clicks etc.)


### In which ways are the individual and the group flowcharts you produced useful?
Both the individual and group flowcharts work as a great tool to create an overview over a complex program. This applies both to a program that already exists as well as to one that is yet to be made. The process of making a flowchart also forces us to reflect upon to whom we want to communicate or explain a program. If the audiences need to understand the overall idea and concept a simpler, more communicative flowcharts will do. If the audience on the other hand are more technically educated - perhaps co-developers on the project - and need to understand how to program functions/should function, a more technically in depth flowchart is more suitable. Our group flowchart is best suited for the former while our individual flowcharts focus more on explaining the technical aspects. Generally, flowcharts are a good tool for cooperation - both in the ideation phase when deciding on an idea, and also when figuring out how to technically create the program and planning who will do what and where to begin. The important part is considering the purpose of the flowchart and then adjusting technical level accordingly. 
