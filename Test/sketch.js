var screen = 0;
var x;
var y;
var score = 0;
var speed;
var rectSpeed;
var rectWidth = 100;
var rectY = 100;
var rectX = -250;

var rectWidth1 = 100;
var rectY1 = 150;

var randomXSpace= 50;

let p ;


function setup() {
  createCanvas(400, 300);
  y = height - 20;
  x = width / 2;

    p = new Player ();
}


function draw() {

  // Display the contents of the current screen
  if (screen == 0) {
    startScreen();
  } else if (screen == 1) {
    gameOn();
    // if the screen variable was changed to 2, show the game over screen
  } else if (screen == 2) {
    gameOver();
  }
}

function mousePressed() {
  if (screen == 0) {
    screen = 1
  } else if (screen == 2) {
    screen = 0
  }
}

function startScreen() {
  Reset();
  background(96, 157, 255)
  fill(255)
  textAlign(CENTER);
  text('WELCOME TO AVOIDING GAME', width / 2, height / 2)
  text('Click to start', width / 2, height / 2 + 20);
}

function gameOn() {
  background(0)

    p.display();
  //move the ellipse
  if (keyIsPressed) {
    if (keyCode === RIGHT_ARROW) {
    p.x += 2;
    } else if (keyCode === LEFT_ARROW) {
    p.x -= 2;
    } else if (keyCode === UP_ARROW) {
      p.y -= 2;
    } else if (keyCode === DOWN_ARROW) {
  p.y += 2;
    }
  }

  rect(rectX, rectY, rectWidth, 20);
  rect(rectX+ randomXSpace, rectY1, rectWidth1, 20);


  rectX += rectSpeed;

  if (rectX > width) {
    randomWidth();
    rectX = -250;
  }

  //if you got to the other side, speed up and go back to the beginning
  if (y < 0) {
    y = height - 20;
    rectSpeed += 1;
    score+=1
  }

  if (y > rectY && y < rectY + 20 && x > rectX && x < rectX + rectWidth) {
    screen = 2
  }
  if (y > rectY1 && y < rectY1 + 20 && x > rectX && x < rectX + rectWidth1) {
    screen = 2
  }
}

function gameOver() {
  background(150)
  textAlign(CENTER);
  text('GAME OVER', width / 2, height / 2)
  text('Click to play again', width / 2, height / 2 + 20);

}

function randomWidth() {
  rectWidth = random(50, 250)
  rectWidth1 = random(50, 250)
}

function randomSpace() {
  randomXSpace = random(50, 100)
}

function Reset() {
  speed = 1;
  score = 0;
  rectSpeed = 5;
  rectY = 100;
  rectX = -100;
	rectY1 = 150;
  y = height - 20;
  x = width / 2;
}

// I create a hero class
class Player {
constructor(x,y,size){
  this.x = width/2;
  this.y = height - 100;
  this.size = (20, 20);


}
display(){
  ellipse (this.x,this.y, this.size)
}
}
