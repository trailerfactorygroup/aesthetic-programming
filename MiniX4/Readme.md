## MiniX4

This week’s topic
This week’s topic is Data Capture.
I start by understanding a concrete definition of the term. What I came up with was that “data capture” is the process of gathering information that will be analyzed and used for a certain purpose later.  This made me understand that we already did some data capturing, when we implemented the functions; MouseX and MouseY, also when we were introduced to mouseIsPressed. The task this week may then be to extend the idea of data capturing by applying more complex data capturing methods.

1. See RunMe [here](http://mathilda.due.gitlab.io/aesthetic-programming/MiniX4/)
2. See code [here](https://gitlab.com/mathilda.due/aesthetic-programming/-/blob/main/MiniX4/sketch.js) 


#### What have I done for my miniX?
Unfortunately, I was sick during the week we worked with the above, therefore I have not made a particularly complex sketch. What I have done is a simple button, that enables you to write your name, and then submit you writing. What happens when you submit is that the program takes a picture of you from your webcam and your name will be displayed all over the screen. Honestly there isn’t a greater conceptualization of the sketch since my sickness has stood in the way of me working intensively with the project. However, I did reflect a lot upon the topic itself.

![](DataCapture.png)

Data capturing is making us question which kind of data is being captured and how is it processed, as well as the consequences of this broader cultural trend known as "datafication."

#### Thoughts about datafication
What is understood by “datafication?
“The term *“datafication” implies that something is made into data.”* [Ulises A. Mejias, Nick Couldry, Datafication 2019]  This is clearly a wide-ranging approach and it makes further questions arise like *“what is ‘the something’ and what is ‘data’”*.   The answer is that *“Data is the “material produced by abstracting the world into categories, measures, and other representational forms [...] that constitute the building blocks from which information and knowledge are created”* [Ulises A. Mejias, Nick Couldry, Datafication 2019]   while “the something” could be literally anything.

So, what I realized was that Datafication, is the process of turning a previously invisible process or activity into data.  And what I find most interesting is how we are able to create data out of human life. You could say that datafication is the quantification of human life through digital information. And despite its ugliness, datafication has become a major tendency in our daily life.

New technologies have opened dozens of new possibilities for "tracking" our everyday activities. Let's say social platforms, such as Facebook or Instagram, collect and monitor data about our friendships to market products and services to us and provide surveillance services to agencies, which in turn influenced our behaviour; promotions that we see on social media daily are also the result of the monitored data.  Because we have more access to the internet through our different linked devices corporations have significantly more power to influence our behaviour. We are facing a future where everything becomes possibly more custom formed as firms combine their growing connectivity with consumers with the ability to gather, harvest, and process customer data at faster speeds.

Today we have a surprising tolerance for frequently accessing citizens' personal information, also known as Big Data. The gradual normalizing of datafication as a new paradigm in science and society may be part of the answer. Not only among technophiles, but also among academics, datafication is becoming a guiding philosophy. Datafication is an ideology that reflects a predominant belief in the objective quantification of human behaviour using internet media technology. Can we deconstruct this ideological foundation of datafication, or is it too late in terms of how integrated it has become in every aspect of our technological behaviour?
